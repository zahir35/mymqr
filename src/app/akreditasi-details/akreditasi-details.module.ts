import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AkreditasiDetailsPage } from './akreditasi-details.page';

const routes: Routes = [
  {
    path: '',
    component: AkreditasiDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AkreditasiDetailsPage]
})
export class AkreditasiDetailsPageModule {}
