import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable } from 'rxjs';

let headers = {
  'Content-Type': 'application/json'
}

@Injectable({
  providedIn: 'root'
})
export class Tab1Service {
 
  constructor(
    private HttpClient: HTTP
  ) { }

  getDataFromAPI(ppt: string, tahap: string, program: string, noruj: string): Promise<any>{
    let linkURL = "https://www2.mqa.gov.my/rest/mqrtest/mqrlisttest/ppt/"+ ppt + "/pptbi/null/tahap/"+ tahap +"/program/"+ program +"/programbi/null/noruj/"+ noruj
    return this.HttpClient.get
      (linkURL, {}, {})
      .then((data) => {
        return JSON.parse(data.data);
      })
      .catch((error) => {
        console.log(error)
      })
  }

  getDataFromAPIInEnglish(searchTerm): Promise<any>{
   return this.HttpClient.get("https://www2.mqa.gov.my/rest/mqrtest/mqrlisttest/ppt/null/pptbi/null/tahap/null/program/null/programbi/null/noruj/null", {}, headers)
         .then((data) => {
        return JSON.parse(data.data);
       })
       .catch((error) => {
         console.log(error)
       })
  
   }
}
