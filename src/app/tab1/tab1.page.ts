import { Component, OnInit } from '@angular/core';
import { Tab1Service } from '../services/tab1.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit {

  ppt: string = null;
  noruja: string = null;
  tahap: string = null;
  namaprogram: string = null;
  pageNumber: number = 1;
  disabledNext: boolean = false;
  indexCount: number = 0;

  dataDocs: any[] = new Array<any>();
  displayData: any[] = new Array<any>();

  starNo: number = 0;
  endNo: number = 10;

  level = false;
  result = false;
  norecord = false;
 
  constructor(
    private searchService: Tab1Service,
    public loadingController: LoadingController,
    public translate: TranslateService,
    public alertCtrl: AlertController,
    public iAB : InAppBrowser
  
  ) {}
  
  getContent() {
    return document.querySelector('ion-content');
  }

  scrollToTop(){
    this.getContent().scrollToTop(500);
  }

  ionViewWillEnter(){
    this.ppt = null;
    this.noruja = null;
    this.tahap = null;
    this.namaprogram = null;
    this.dataDocs = new Array<any>();
    this.displayData = new Array<any>();
    this.norecord = null;

  }

  ngOnInit() {}

  // chgL(L: string){
  //   this.translate.use(L)

  // }

  isEn = true;
  chgL() {
    this.isEn = !this.isEn;
    this.isEn ? this.translate.use('en') : this.translate.use('my');  
  } 


  prev() {
    this.starNo  = this.starNo-10;
    this.endNo = this.endNo-10

    this.displayData = this.dataDocs.filter( 
      (e,index)=> {
        for( let i = this.starNo ; i<this.endNo ; i++)
        {
          if(index == i){
            return e;
          }
        }
      }
    )
  }

  next() {
    this.starNo = this.starNo+10;
    this.endNo = this.endNo+10
    console.log(this.starNo)
    this.displayData = this.dataDocs.filter( 
      (e,index)=> {
        for( let i = this.starNo ; i<this.endNo ; i++)
        {
          if(index == i){
            return e;
          }
        }
      }
    )
  }

  async doSearch() {
    const loading = await this.loadingController.create({
      // message: 'Loading',
    });
    await loading.present();

    this.searchService.getDataFromAPI(this.ppt, this.tahap, this.namaprogram, this.noruja).then(
      data => {
        this.starNo = 0;
        this.dataDocs = data;
        console.log("searchService")
        console.log(data)
        if (this.dataDocs.length > 0){

          this.result=true;
          this.norecord=false;
          this.displayData = this.dataDocs.filter( 
            (e,index)=> {
              for( let i = this.starNo ; i<this.endNo ; i++)
              {
                if(index == i){
                  return e;
                }
              }
            }
          )
         }else{

           this.norecord=true;
           this.result=false;
           this.displayData = [];
         }
         loading.dismiss()

        //  console.log(this.dataDocs.length)

        //  this.displayData = this.dataDocs.splice(this.starNo, this.endNo)
      },
      error=>{
        loading.dismiss()
        this.norecord=true;
        this.result=false;
        this.displayData = [];
        console.log(error)
      }
    )
  }

  goToBrowser(){

    this.iAB.create('https://www2.mqa.gov.my/mqr', "_system");

  }
  
}

