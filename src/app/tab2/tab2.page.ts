import { Component, OnInit } from '@angular/core';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { TranslateService } from '@ngx-translate/core';
import { File } from '@ionic-native/file/ngx';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  constructor(private document: DocumentViewer, private file: File,
    public translate: TranslateService) {}

  openPDF(){
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }
   
    this.file.applicationDirectory + 'www/assets/mqf2019.pdf'

    this.document.viewDocument(this.file.applicationDirectory + 'www/assets/mqf2019.pdf', 'application/pdf', options) 
  }


  // this.file.applicationDirectory + 'www/assets/mqf2019.pdf'

  
  ngOnInit() {
  }

  isEn = true;
  chgL() {
    this.isEn = !this.isEn;
    this.isEn ? this.translate.use('en') : this.translate.use('my');
  } 
  
}



