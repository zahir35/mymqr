import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(
    public translate: TranslateService
  ) {}

  ngOnInit() {
  }

  // chgL(L: string){
  //   this.translate.use(L)
  // }

  isEn = true;
  chgL() {
    this.isEn = !this.isEn;
    this.isEn ? this.translate.use('en') : this.translate.use('my');
  } 

}
