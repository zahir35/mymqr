import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  constructor(
    public translate: TranslateService,
    public iAB : InAppBrowser
  ) { }

  ngOnInit() {
  }

  // chgL(L: string){
  //   this.translate.use(L)
  // }

  isEn = true;
  chgL() {
    this.isEn = !this.isEn;
    this.isEn ? this.translate.use('en') : this.translate.use('my');  
  } 

  goToBrowser(){

    this.iAB.create('https://www2.mqa.gov.my/mqr', "_system");

  }


}
