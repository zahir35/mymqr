import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit{

  backSub: any
  public counter = 0

  constructor(
    public translate: TranslateService,
    private toastCtrl: ToastController,
    platform: Platform

  ) {this.backSub = platform.backButton.subscribeWithPriority(666666,
    () => {
      if (this.counter == 0) {
        this.counter++;
        this.presentToast();
        setTimeout(() => { this.counter = 0 }, 3000)
      } else {
        navigator["app"].exitApp()
      }
    }
  )}
  

  ngOnInit() {

  }

  chgL(L: string){
    this.translate.use(L)

  }
  
  async presentToast() {
    let message = "";
    this.translate.get("exit").subscribe( val => {
      message = val;
    })
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  

}
